"""Crow/AAP Alarm IP Module Status Definition"""

COMMANDS = {
    "arm": "ARMAWAY",
    "stay": "ARMSTAY",
    "disarm": "DISARM",  # Add ' codeE'
    "get_memory_events": "MEME",
    "exit_memory_events": "MEMX",
    "cancel": "CANCEL",  # Will cancel any ongoing RS232 activity such as MEM command output.
    "panic": "KEY N", # as per etx protocol manual
    "toggle_chime": "CHIME",
    "quick_arm_a": "A",
    "quick_arm_b": "B",
    "relay_1_on": "RL1",
    "relay_2_on": "RL2",
    # "toogle_output_x": "OO",
    "output_on": "OUTPUTON",
    "output_off": "OUTPUTOFF",
    "output_state": "OUTPUT",
    "keys": "KEYS",  # The KEYS command allows entry of data into a virtual keypad.
    "status": "STATUS",  # Will send the current system status.
    "version": "VERSION ?",
}

RESPONSE_FORMATS = {
    # ZONE MESSAGES
    "ZO(?P<data>\d+)": {
        "name": "Zone Open",
        "handler": "zone_state_change",
        "attr": "open",
        "status": True,
    },
    "ZC(?P<data>\d+)": {
        "name": "Zone Closed",
        "handler": "zone_state_change",
        "attr": "open",
        "status": False,
    },
    "ZA(?P<data>\d+)": {
        "name": "Zone Alarm",
        "handler": "zone_state_change",
        "attr": "alarm",
        "status": True,
    },
    "ZR(?P<data>\d+)": {
        "name": "Zone Alarm Restore",
        "handler": "zone_state_change",
        "attr": "alarm",
        "status": False,
    },
    "ZBY(?P<data>\d+)": {
        "name": "Zone Bypass",
        "handler": "zone_state_change",
        "attr": "bypass",
        "status": True,
    },
    "ZBYR(?P<data>\d+)": {
        "name": "Zone Bypass Restore",
        "handler": "zone_state_change",
        "attr": "bypass",
        "status": False,
    },
    "ZTA(?P<data>\d+)": {
        "name": "Zone Tamper",
        "handler": "zone_state_change",
        "attr": "tamper",
        "status": True,
    },
    "ZTR(?P<data>\d+)": {
        "name": "Zone Tamper Restore",
        "handler": "zone_state_change",
        "attr": "tamper",
        "status": False,
    },
    # AREA MESSAGES
    "A1": {
        "name": "Area 1 Armed",
        "handler": "area_state_change",
        "attr": "armed",
        "area": "1",
        "status": True,
    },
    "A2": {
        "name": "Area 2 Armed",
        "handler": "area_state_change",
        "attr": "armed",
        "area": "2",
        "status": True,
    },
    "S1": {
        "name": "Area 1 Stay Armed",
        "handler": "area_state_change",
        "attr": "stay_armed",
        "area": "1",
        "status": True,
    },
    "S2": {
        "name": "Area 2 Stay Armed",
        "handler": "area_state_change",
        "attr": "stay_armed",
        "area": "2",
        "status": True,
    },
    "D1": {
        "name": "Area 1 Disarmed",
        "handler": "area_state_change",
        "attr": "disarmed",
        "area": "1",
        "status": True,
    },
    "D2": {
        "name": "Area 2 Disarmed",
        "handler": "area_state_change",
        "attr": "disarmed",
        "area": "2",
        "status": True,
    },
    "EA1": {
        "name": "Area 1 Exit Delay",
        "handler": "area_state_change",
        "attr": "exit_delay",
        "area": "1",
        "status": True,
    },
    "EA2": {
        "name": "Area 2 Exit Delay",
        "handler": "area_state_change",
        "attr": "exit_delay",
        "area": "2",
        "status": True,
    },
    "ES1": {
        "name": "Area 1 Stay Exit Delay",
        "handler": "area_state_change",
        "attr": "stay_exit_delay",
        "area": "1",
        "status": True,
    },
    "ES2": {
        "name": "Area 2 Stay Exit Delay",
        "handler": "area_state_change",
        "attr": "stay_exit_delay",
        "area": "2",
        "status": True,
    },
    # OUTPUT MESSAGES
    "OO(?P<data>\d+)": {
        "name": "Output On",
        "handler": "output_state_change",
        "attr": "open",
        "status": True,
    },
    "OR(?P<data>\d+)": {
        "name": "Output Off",
        "handler": "output_state_change",
        "attr": "open",
        "status": False,
    },
    # SYSTEM MESSAGES
    "MF": {
        "name": "Mains Fail",
        "handler": "system_state_change",
        "attr": "mains",
        "status": False,
    },
    "MR": {
        "name": "Mains Restore",
        "handler": "system_state_change",
        "attr": "mains",
        "status": True,
    },
    "BF": {
        "name": "Battery Fail",
        "handler": "system_state_change",
        "attr": "battery",
        "status": False,
    },
    "BR": {
        "name": "Battery Restore",
        "handler": "system_state_change",
        "attr": "battery",
        "status": True,
    },
    "TA": {
        "name": "Tamper Fail",
        "handler": "system_state_change",
        "attr": "tamper",
        "status": False,
    },
    "TR": {
        "name": "Tamper Restore",
        "handler": "system_state_change",
        "attr": "tamper",
        "status": True,
    },
    "LF": {
        "name": "Line Fail",
        "handler": "system_state_change",
        "attr": "line",
        "status": False,
    },
    "LR": {
        "name": "Line Restore",
        "handler": "system_state_change",
        "attr": "line",
        "status": True,
    },
    "DF": {
        "name": "Dialler Fail",
        "handler": "system_state_change",
        "attr": "dialler",
        "status": False,
    },
    "DR": {
        "name": "Dialler Restore",
        "handler": "system_state_change",
        "attr": "dialler",
        "status": True,
    },
    "RO": {
        "name": "Ready On",
        "handler": "system_state_change",
        "attr": "ready",
        "status": True,
    },
    "NR": {
        "name": "Not Ready",
        "handler": "system_state_change",
        "attr": "ready",
        "status": False,
    },
    "FF": {
        "name": "Fuse Fail",
        "handler": "system_state_change",
        "attr": "fuse",
        "status": False,
    },
    "FR": {
        "name": "Fuse Restore",
        "handler": "system_state_change",
        "attr": "fuse",
        "status": True,
    },
    "ZBF": {
        "name": "Zone Battery Fail",
        "handler": "system_state_change",
        "attr": "zonebattery",
        "status": False,
    },
    "ZBR": {
        "name": "Zone Battery Restore",
        "handler": "system_state_change",
        "attr": "zonebattery",
        "status": True,
    },
    "PBF": {
        "name": "Pendant Battery Fail",
        "handler": "system_state_change",
        "attr": "pendantbattery",
        "status": False,
    },
    "PBR": {
        "name": "Pendant Battery Restore",
        "handler": "system_state_change",
        "attr": "pendantbattery",
        "status": True,
    },
    "CTF": {
        "name": "Code Tamper Fail",
        "handler": "system_state_change",
        "attr": "codetamper",
        "status": True,
    },
    "CTR": {
        "name": "Code Tamper Restore",
        "handler": "system_state_change",
        "attr": "codetamper",
        "status": False,
    },
    # OTHER MESSAGES
    "Welcome": {
        "name": "ESX Communication Check",
        "handler": "system_state_change",
        "attr": "comms_status",
        "status": "COMMS_CONNECTING",
    },
    "OK Status": {
        "name": "ESX Communication Check",
        "handler": "system_state_change",
        "attr": "comms_status",
        "status": "COMMS_CONNECTED",
    },
    "OK (?P<data>.*)": {
        "name": "ESX Command Response",
        "handler": "system_state_change",
        "attr": "command_response",
        "status": "SUCCESS",
    },
    "ERROR (?P<data>.*)": {
        "name": "ESX Command Response",
        "handler": "system_state_change",
        "attr": "command_response",
        "status": "FAILURE",
    },
    "ERR (?P<data>.*)": {
        "name": "ESX Command Response",
        "handler": "system_state_change",
        "attr": "command_response",
        "status": "FAILURE",
    },
    "Bye\.": {
        "name": "ESX closing data connection",
        "handler": "system_state_change",
        "attr": "comms_status",
        "status": "COMMS_DISCONNECT_DEVICE_CONFIRMED",
    },
}
